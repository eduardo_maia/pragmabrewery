const express = require("express");
const app = express();
const bodyParser = require("body-parser");
var fs = require("fs");
var path = require('path');


app.listen(process.env.PORT || 3000, () => {
  console.log("listening on 3000");
});

app.set('views', './src/views');
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "../public")));
app.use(express.static(path.join(__dirname, "../views")));
app.use(express.static(path.join(__dirname, "../style")));
app.use(express.static(path.join(__dirname, "../js")));
app.use(express.static("node_modules"));
app.use(express.static("data"));


app.get("/", (req, res) => {
  var jsonData = fs.readFileSync(path.join(__dirname, '../data/beer.json'));
  var beerList = JSON.parse(jsonData);
  res.render("index.ejs", { beer: beerList });
});

// http methods
app.post("/beer", (req, res) => {});
app.put("/beer", (req, res) => {});
app.delete("/beer", (req, res) => {});
