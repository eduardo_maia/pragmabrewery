lmdd.set(document.getElementById("drag-drop"), {
  containerClass: "nestable",
  draggableItemClass: "nested-item",
  positionDelay: true
});

function checkBeersTemperature(element) {
  const container = element.detail.to.container.parentNode;
  if (container.getAttribute("id") == "available-beers-container") {
    makeBeersAvailable(container);
  } else {
    checkBeersTemperatureByContainer(container);
  }
}

function checkBeersTemperatureByContainer(container) {
  const temperature = Array.from(
    container.getElementsByClassName("container-temperature")
  )[0].value;

  const beers = Array.from(container.getElementsByClassName("beer"));
  beers.forEach(beer => {
    validateTemperature(beer, temperature);
  });
}

function validateTemperature(beer, container_temp) {
  const minTemperature = Array.from(
    beer.getElementsByClassName("beer-min-temperature")
  )[0].getAttribute("value");

  const maxTemperature = Array.from(
    beer.getElementsByClassName("beer-max-temperature")
  )[0].getAttribute("value");

  if (container_temp > maxTemperature) {
    beer.setAttribute("data-attribute-color", "red") 
  } else if (container_temp < minTemperature) {
    beer.setAttribute("data-attribute-color", "blue") 
  } else {
    beer.setAttribute("data-attribute-color", "gold") 
  }
}

function onChangeTemperature(element) {
  const container = element.parentNode;
  const output = Array.from(
    container.getElementsByClassName("temperature-output")
  )[0];

  const temperature = element.value;
  output.value = temperature;
  checkBeersTemperatureByContainer(container);
}

function makeBeersAvailable(container) {
  const beers = Array.from(container.getElementsByClassName("beer"));
  beers.forEach(beer => {
    beer.setAttribute("data-attribute-color", "gold") 
  });
}


//controller.js




// eventListeners
document.addEventListener("lmddend", checkBeersTemperature);
