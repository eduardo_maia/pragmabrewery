# Pragma Brewery Challenge

# Application

- The application is responsible for helping the truck driver to monitor the temperature of car loaded beer containers;
- The Application has drag and drop components to simulate the stock of beers and the components, so you can drag the beers and put in the container of your choice;
- Immediately after receiving the beer, the temperature is validated and changes the color as the temperature changes inside the container;
- The beer returns to its normal temperature when it is put into the stock again;

## Setup

- You must have yarn/node installed
- To run, go to the project folder and type `$ yarn start` for starting the development server.
- After that, the server will start at `https://localhost:3000`

## What are the highlights of your logic/code writing style?

- I've created using JavaScript whithot any franmework.
- I used LMDD and MaterializeCss libraries to make the pl more user friendly
- I've created a backend server with Node.js to let the application ready for future integrations(API, DB)
- The application is responsive.

## What could have been done in a better way? What would you do in version 2.0?

- A webservice can be contructed to send information about the beers load to the sender and receiver
- Other kind of notification could be created to show to driver.
- Save data in a real database to filter data by time;
- Next version could be improved to use React to simplify the user interface;
- Integration tests;

## What were the questions you would ask and your own answers/assumptions?

- To simplify, I assumed that show warning temperatures through colors is the best way for a driver.
- I've assumed that the containers must accept from none to all of the beers